import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListIpComponent } from './list-ip.component';

describe('ListIpComponent', () => {
  let component: ListIpComponent;
  let fixture: ComponentFixture<ListIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
