import { Injectable } from '@angular/core';
import { WhiteList } from '../Classes/WhiteList';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WhiteListService {
  IpList: WhiteList[] = [];
  BASIC_URL = "Services/api/WhiteList/";
  constructor(private http: HttpClient) { }

  addIp(WhiteList: WhiteList): Observable<any> {
    return this.http.post(this.BASIC_URL + "AddIP", [WhiteList]);
  }
  UpdateIp(ip: WhiteList) {
    return this.http.post(this.BASIC_URL + "EditDescription",[ip])
  }
  DeleteIp(ip: WhiteList) {
    return this.http.post(this.BASIC_URL + "DeleteIp" , [ip])
  }
  GetIpList() {
    return this.http.get(this.BASIC_URL + "GetAllList")
  }
}
