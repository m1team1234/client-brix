# Client brix

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.

### Steps to use this repository

* Clone the brix client side repository to empty folder.
* Ran the command 'npm install' in the terminal of visual code.
* Ran the command 'ng serve -o --proxy-config proxy-config.json' in the terminal and see how it work together.